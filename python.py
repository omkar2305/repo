# Importing relevant libraries
from tkinter import *
import random

# to create a window
root = Tk(className = " Rock-Paper-Scissor")

#root.title("Rock-Paper-Scissor")

root.iconbitmap(r"rps.ico")

root.resizable(height = False, width = False)

click = True

# assigning variables to all the images
rhand_image = PhotoImage(file = "rHand.png")
phand_image = PhotoImage(file = "pHand.png")
shand_image = PhotoImage(file = "sHand.png")
rock_image = PhotoImage(file = 'rock.png')
angryrock_image = PhotoImage(file = 'angry-rock.png')
paper_image = PhotoImage(file = 'paper.png')
angrypaper_image = PhotoImage(file = 'angry-paper.png')
scissor_image = PhotoImage(file = 'sissors.png')
angryscissor_image = PhotoImage(file = 'angry-sissors.png')
win_image = PhotoImage(file = 'youwin.png')
loose_image = PhotoImage(file = 'youlose.png')
tie_image = PhotoImage(file = 'tied.png')

# assigning empty string to label and buttin variables
label_1 = ''
label_2 = ''
label_3 = ''
rhand_button = ''
phand_button = ''
shand_button = ''

#create function that define basic gui of the game
def start():

    #this buttons can be used anywhere in the program
    global label_1, label_2, label_3
    global rhand_button, phand_button, shand_button

    label_1 = Label(root, text = "Rock", fg = "white", bg = "black")
    label_2 = Label(root, text = "Paper", fg = "white", bg = "black")
    label_3 = Label(root, text = "Scissor", fg = "white", bg = "black")

    rhand_button = Button(root, image = rhand_image, command = lambda:you_pick('rock'))
    phand_button = Button(root, image = phand_image, command = lambda:you_pick('paper'))
    shand_button = Button(root, image = shand_image, command = lambda:you_pick('scissor'))

    # assigning position to the labels and buttons
    label_1.grid(row = 0, column = 0)
    label_2.grid(row = 0, column = 1)
    label_3.grid(row = 0, column = 2)
    rhand_button.grid(row = 1, column = 0)
    phand_button.grid(row = 1, column = 1)
    shand_button.grid(row = 1, column = 2)

# create a function so that computer randomly chooses from the list
def comp_choice():
    computer = random.choice(['rock','paper','scissor'])
    return computer

# create function to implement the conditions of the game
def you_pick(your_choice):

    global click

    comp = comp_choice()
    if click == True:
        label_1.configure(text = "Player_Choice")
        label_2.configure(text = "Result")
        label_3.configure(text = "Computer_Choice")
        if comp == 'rock':
            if your_choice == 'rock':
                rhand_button.configure(image = rock_image)
                phand_button.configure(image = tie_image)
                shand_button.configure(image = rock_image)
                click = False

            elif your_choice == 'paper':
                rhand_button.configure(image = paper_image)
                phand_button.configure(image = win_image)
                shand_button.configure(image = angryrock_image)
                click = False

            else:
                rhand_button.configure(image = angryscissor_image)
                phand_button.configure(image = loose_image)
                shand_button.configure(image = rock_image)
                click = False

        elif comp == 'paper':
            if your_choice == 'rock':
                rhand_button.configure(image = angryrock_image)
                phand_button.configure(image = loose_image)
                shand_button.configure(image = paper_image)
                click = False

            elif your_choice == 'paper':
                rhand_button.configure(image = paper_image)
                phand_button.configure(image = tie_image)
                shand_button.configure(image = paper_image)
                click = False

            else:
                rhand_button.configure(image = scissor_image)
                phand_button.configure(image = win_image)
                shand_button.configure(image = angrypaper_image)
                click = False

        else:
            if your_choice == 'rock':
                rhand_button.configure(image = rock_image)
                phand_button.configure(image = win_image)
                shand_button.configure(image = angryscissor_image)
                click = False

            elif your_choice == 'paper':
                rhand_button.configure(image = angrypaper_image)
                phand_button.configure(image = loose_image)
                shand_button.configure(image = scissor_image)
                click = False

            else:
                rhand_button.configure(image = scissor_image)
                phand_button.configure(image = tie_image)
                shand_button.configure(image = scissor_image)
                click = False
    else:
        label_1.configure(text = "Rock")
        label_2.configure(text = "Paper")
        label_3.configure(text = "Scissor")
        rhand_button.configure(image = rhand_image)
        phand_button.configure(image = phand_image)
        shand_button.configure(image = shand_image)
        click = True


start()
root.mainloop()